import { Deserializable } from "./deserializable.model";

export class User implements Deserializable {
    
    id: number;
    name: string;
    surname: string;
    email: string;
    phone: string;
    role: string;

    deserializable(input: any) {
        Object.assign(this, input);
        return this;
    }
}
