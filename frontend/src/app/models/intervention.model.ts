import { Deserializable } from "./deserializable.model";

export class Intervention implements Deserializable {
    
    id: number;
    adress: string;
    orientation: string;
    bat: string;

    deserializable(input: any) {
        Object.assign(this, input);
        return this;
    }
}
