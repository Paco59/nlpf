import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "../services/auth.service";
import { AgendaService } from "../services/agenda.service";

const monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUNE",
  "JULY", "AUG", "SEP", "OCT", "NOV", "DEC"
];

const dayNames = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday",
  "Sonday"
];

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css'],
  providers: [AgendaService],
})

export class AgendaComponent implements OnInit {

  constructor(private agendaService: AgendaService,
              private auth: AuthService,
              private router: Router) { }

  viewDate: Date = new Date();
  events: any[] = [];
  
  ngOnInit() {
    if (!this.auth.session) return this.router.navigate(['login']);
    this.agendaService.get().subscribe(response => {
      //this.events.push(elt);
      console.log(JSON.stringify(response));
      response.forEach(res => {
        var ev = res;
        ev.start = new Date(ev.start);
        ev.end = new Date(ev.end);
        ev.startDay = ev.start.getDate();
        ev.startHour = ev.start.getHours() + ':' + ev.start.getMinutes();
        ev.endHour = ev.end.getHours() + ':' + ev.end.getMinutes();
        ev.month = monthNames[ev.start.getMonth()];
        ev.day = dayNames[ev.start.getDay()];
        this.events.push(ev);
      });
      console.log(JSON.stringify(this.events, null, 2));
    })
  }

}

