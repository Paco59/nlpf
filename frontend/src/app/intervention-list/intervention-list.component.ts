import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { InterventionService } from "./../services/intervention.service";
import { AuthService } from "./../services/auth.service";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'app-intervention-list',
  templateUrl: './intervention-list.component.html',
  styleUrls: ['./intervention-list.component.css']
})
export class InterventionListComponent implements OnInit {

  interventions : any = [];
  states: string[] = ["en attente", "en cours", "refusé", "finis"]
  chosen: any = {};
  orientations : string[] = ["nord", "sud", "est", "ouest", "nord-est", "nord-ouest", "sud-est", "sud-ouest", 
  "nord-nord-est", "nord-est-est", "nord-nord-ouest", "nord-ouest-ouest", "sud-sud-est",
  "sud-est-est", "sud-sud-ouest", "sud-ouest-ouest"];

  constructor(private router: Router,
              private inter: InterventionService,
              private auth: AuthService) { }

  ngOnInit() {
    if (!this.auth.session) return this.router.navigate(['login']);
    this.init();
  }

  canBeModified(state: string) {
    return this.auth.session.role != 'admin' && state == 'en attente';
  }

  canBeReAsk(state: string) {
    return this.auth.session.role != 'admin' && state == 'refusé';
  }

  validate(int: any) {
    this.inter.accept(int).subscribe(data => {
      this.init();
    })
  }

  openModal(int: any) {
    this.chosen = int;
  }

  reAsk(int: any) {
    int.etat = 'en attente';
    this.inter.edit(int).subscribe(data => {
      this.init();
    })
  }

  refuse() {
    this.inter.refuse(this.chosen).subscribe(data => {
      this.init();
    })

  }

  save(int: any) {
    this.inter.edit(int).subscribe(data => {
      this.init();
    })
  }

  end(int: any) {
    this.inter.end(int).subscribe(data => {
      this.init();
    })
  }

  init() {
    this.inter.list().subscribe(data => {
      this.interventions = data;
      this.interventions.map((todo, i)=>{
        todo.photo = "http://localhost:3000/intervention/" + todo.photo;
      });
    });
  }
}
