import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  constructor(private http: HttpClient) { }

  get() {
    //console.log(inter);
    return this.http.get<any>('/agenda/list').pipe(map(data => {
      return data;
    }));
  }
}
