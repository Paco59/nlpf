import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class InterventionService {

  constructor(private http: HttpClient) { }

  create(inter: FormData) {
    console.log(inter);
    return this.http.post<any>('/intervention/add', inter, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map(data => {
      console.log(data);
      return data;
    }));
  }

  list() {
    return this.http.get<any>('/intervention/list').pipe(map(data => {
      return data;
    })) 
  }

  accept(inter: any) {
    return this.http.put<any>('/intervention/accept', inter).pipe(map(data => {
      return data;
    }))
  }

  refuse(inter: any) {
    return this.http.put<any>('/intervention/refused', inter).pipe(map(data => {
      return data;
    }))
  }

  edit(inter: any) {
    return this.http.put('/intervention/update', inter).pipe(map(data => {
      return data;
    }))
  }

  end(inter: any) {
    return this.http.put<any>('/intervention/end', inter).pipe(map(data => {
      return data;
    }))
  }
}
