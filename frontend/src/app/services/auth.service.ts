import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { User } from "./../models/user.model";
import { UserComponent } from "./../user/user.component";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  session : any = JSON.parse(localStorage.getItem('currentUser')) || null;
  token : string = localStorage.getItem('token') || null;

  constructor(private http: HttpClient) { }

  login(email: string, password: string) {
    // Already conencted
    if (this.session) return this.session.user;
    return this.http.post<any>('/auth/signin', { email: email, password: password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user.user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user.user));
          localStorage.setItem('token', JSON.stringify(user.token));
          this.session = JSON.parse(localStorage.getItem('currentUser'));
          this.token = localStorage.getItem('token');
        }
        return user;
      }));
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    this.session = null;
    this.token = null;
  }
}
