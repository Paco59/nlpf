import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { User } from "./../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  create(user: any) {
    return this.http.post('/auth/signup', user);
  }



}
