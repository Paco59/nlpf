import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { Routes, RouterModule} from "@angular/router";
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AuthInterceptor } from "./request.interceptor";
import { InterventionComponent } from './intervention/intervention.component';
import { InterventionListComponent } from './intervention-list/intervention-list.component';
import {AgendaComponent} from "./agenda/agenda.component";

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'intervention', component: InterventionComponent},
  {path: 'intervention/list', component: InterventionListComponent},
  {path: 'agenda', component: AgendaComponent},

];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    RegisterComponent,
    LoginComponent,
    InterventionComponent,
    InterventionListComponent,
    AgendaComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    //AppRoutingModule,
    RouterModule.forRoot(routes),
    FormsModule,
    BrowserAnimationsModule,
  ],
  providers: [
    HttpClientModule,
    HttpClient,
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: AuthInterceptor
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
