import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InterventionService } from "./../services/intervention.service";
import { AuthService } from "./../services/auth.service";

@Component({
  selector: 'app-intervention',
  templateUrl: './intervention.component.html',
  styleUrls: ['./intervention.component.css']
})
export class InterventionComponent implements OnInit {

  file : File = null;
  model : any = {};
  orientations : string[] = ["nord", "sud", "est", "ouest", "nord-est", "nord-ouest", "sud-est", "sud-ouest", 
  "nord-nord-est", "nord-est-est", "nord-nord-ouest", "nord-ouest-ouest", "sud-sud-est",
  "sud-est-est", "sud-sud-ouest", "sud-ouest-ouest"];

  constructor(private interService: InterventionService,
              private auth: AuthService,
              private router: Router) { }

  ngOnInit() {
    if (!this.auth.session) return this.router.navigate(['login']);
  }

  onSelectedFile(event) {
    this.file = <File>event.target.files[0];
  }

  create() {
    var fd = new FormData();
    if (this.file && this.file.name)
      fd.append('file', this.file, this.file.name);
    let keys = Object.keys(this.model);
    keys.forEach(k => {
      fd.append(k, this.model[k]);
    });
    this.interService.create(fd).subscribe(inter => {
      console.log("TEST", inter);
    });
  }

}
