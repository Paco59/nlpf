import { AuthService } from './services/auth.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
 
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
 
  constructor(private auth: AuthService) {}
 
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // Get the auth token from the service.
    const authToken = JSON.parse(this.auth.token) || "";
 
    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    const authReq = req.clone({
      setHeaders: {'Authorization': `Bearer ${authToken}`},
      url: `http://127.0.0.1:3000${req.url}`
    });
    console.log("[INTERCEPTOR]", authReq, authToken);
    
    
    // send cloned request with header to the next handler.
    return next.handle(authReq);
  }
}