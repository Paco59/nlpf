import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from "./../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  returnUrl: string;
  error: string;

  constructor(private router: Router, private route: ActivatedRoute, private auth: AuthService) { }

  ngOnInit() {
    if (this.auth.session) return this.router.navigate(['intervention']);
    this.auth.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.auth.login(this.model.email, this.model.password).subscribe(data => {
      this.router.navigate(['intervention']);
      this.error = null;
    }, err => {
      console.log(err);
      this.error = JSON.stringify(err.error.error);
    })
  }
}
