var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cors = require('cors');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var _ = require('lodash');

var routes = require('./routes');

const env = process.env.NODE_ENV || 'development';
global.__basedir = __dirname;
var app = express();
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

_.forEach(routes, route => {
  app.use(route.path, route.router);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.listen(3000, () => {
  console.log("Server listening on port %d", 3000);
})

module.exports = app;
