'use strict';

var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var basename = path.basename(__filename);
var db = {};

const env = process.env.NODE_ENV || 'development';
const configDB = process.env.DB_URL || require('./../config/db.json')[env];

const sequelize = new Sequelize(configDB);

sequelize.authenticate().then(() => {
    console.log('Connection established successfully.');
    sequelize.sync({
        force: false
    });
})

fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        var model = sequelize['import'](path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.Sequelize = Sequelize;
module.exports = db;
