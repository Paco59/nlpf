'use strict';
module.exports = (sequelize, DataTypes) => {

    const Agenda = sequelize.define('Agenda', {
        start: DataTypes.DATE,
        end: DataTypes.DATE,
    });

    Agenda.associate = function (models) {
        models.Agenda.belongsTo(models.Ticket);
    };
    return Agenda;
};