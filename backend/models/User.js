'use strict';
module.exports = (sequelize, DataTypes) => {

    const User = sequelize.define('User', {
        name: DataTypes.STRING,
        surname: DataTypes.STRING,
        email: { type: DataTypes.STRING, unique: true},
        phone: DataTypes.STRING,
        password: DataTypes.STRING,
        salt: DataTypes.STRING,
        role: { type: DataTypes.STRING, defaultValue: "user" },
        blacklisted: {type : DataTypes.BOOLEAN, defaultValue : false}
    });


    return User;
};
