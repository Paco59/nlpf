'use strict';
module.exports = (sequelize, DataTypes) => {

    const Ticket = sequelize.define('Ticket', {
        adress: DataTypes.STRING,
        bat: DataTypes.STRING,
        orientation: DataTypes.ENUM("nord", "sud", "est", "ouest", "nord-est", "nord-ouest", "sud-est", "sud-ouest", 
                                    "nord-nord-est", "nord-est-est", "nord-nord-ouest", "nord-ouest-ouest", "sud-sud-est",
                                    "sud-est-est", "sud-sud-ouest", "sud-ouest-ouest"),
        etat:{
            type: DataTypes.ENUM,
            values: ["en attente", "en cours", "finis", "refusé"],
            defaultValue: "en attente"
        },
        photo: DataTypes.STRING,
        raison: DataTypes.STRING
    });

    Ticket.associate = function (models) {
        models.Ticket.belongsTo(models.User)
    };
    return Ticket;
};