var model = require('./../models');
var agenda = require('./agenda');
var nodemailer = require('nodemailer');
var intervention = model.Ticket;
var User = model.User;

var path = require('path');
var fs = require('fs');

const handleError = (err, res) => {
    res.status(500)
        .contentType("text/plain")
        .end("Oops! Something went wrong!");
};

var mailtextgood = "Bonjour, \n" +
    "intervention plannifier, merci de consulter votre intervention\n" +
    "cordialement bob";
var transporter = nodemailer.createTransport({
    host: 'smtp.office365.com',
    port: 587,
    secure: false,
    auth: {
        user: 'francois.forestier@epita.fr',
        pass: ''
    },
    tls: {
        ciphers: 'SSLv3'
    },
    requireTLS: true
});

var sendMail = (msg, to) => {
    User.findById(to).then(res => {
        console.log("User", res);
        var mailOptions = {
            from: 'francois.forestier@epita.fr',
            to: res.email,
            subject: 'Notification de BOB Le Bricoleur',
            text: msg
        }
        transporter.sendMail(mailOptions, function (err, info) {
            if (err) console.log(err);
            console.log("Email sent");
        })

    })
}

function upload(req, res, next) {
    const tempPath = req.file.path;
    const targetPath = "./uploads/" + req.file.originalname;
    if (req.file.mimetype === "image/png" || req.file.mimetype === "image/jpeg") {
        fs.rename(tempPath, targetPath, err => {
            if (err) return handleError(err, res);
            return next;
        });
    } else {
        fs.unlink(tempPath, err => {
            if (err) return handleError(err, res);

            res
                .status(403)
                .contentType("text/plain")
                .end("Only .png & .jpeg files are allowed!");
        });
    }
}

module.exports.create = (req, res, next) => {
    // console.log(req.file);
    var inter = req.body;
    inter.UserId = req.body.user.id;
    if (req.file) {
        upload(req, res, this);
        inter.photo = req.file.originalname;
    }
    intervention.create(inter).then((result) => {
        return res.status(200).send({
            reulstat: result
        });
    }).catch((err) => {
        return res.status(500).json(err);
    })
};

module.exports.accepted = (req, res, next) => {
    let model = {
        etat: "en cours"
    }
    var date = agenda.add(req, res, next);
    console.log(date);
    intervention.update(model, {
        where: {
            id: req.body.id
        }
    }).then(result => {
        console.log(result);

        sendMail( mailtextgood, req.body.UserId);
        res.status(200).json(result);
    }).catch(err => {
        res.status(500).json(err);
    });
};

module.exports.end = (req, res, next) => {
    let model = {
        etat: "finis"
    }
    intervention.update(model, {
        where: {
            id: req.body.id
        }
    }).then(result => {
        res.status(200).json(result);
    }).catch(err => {
        res.status(500).json(err);
    });
};

module.exports.refused = (req, res, next) => {
    let model = {
        etat: "refusé",
        raison: req.body.raison
    }
    intervention.update(model, {
        where: {
            id: req.body.id
        }
    }).then(result => {
        sendMail( "Bonjour, \n Intervention refusé car " + req.body.raison + "\n Cordialement Bob", req.body.UserId);
        res.status(200).json(result);
    }).catch(err => {
        res.status(500).json(err);
    });
};

module.exports.edit = (req, res, next) => {
    intervention.findById(req.body.id).then(result => {
        if (result.etat !== "en attente") {
            res.status(403).json({
                err: "cannot edit view intervention"
            });
        } else {
            intervention.update(req.body, {
                where: {
                    id: req.body.id
                }
            }).then(result => {
                res.status(200).json(result);
            }).catch(err => {
                res.status(500).json(err);
            });
        }
    }).catch(err => {
        res.status(500).json(err);
    });
};


module.exports.list = (req, res, next) => {
    //console.log(req.body.user);*
    var Where = {};
    if (req.body.user.role == "user") {
        Where = {
            where: {
                UserId: req.body.user.id
            }
        }
    }
    intervention.findAll(Where).then(result => {
        res.status(200).json(result);
    }).catch(err => {
        res.status(500).json(err);
    });
};