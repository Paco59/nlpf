/**
 * The purpose of this index.js to require all controllers files in one.
 */

var controllers = {};

// Add filename of controllers
var files = ['user', 'intervention', 'agenda', 'auth'];

for (var i = 0; i < files.length; i++) {
    controllers[files[i]] = require('./' + files[i]);
}

module.exports = controllers;