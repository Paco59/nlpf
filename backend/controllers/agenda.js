var sequalize = require("sequelize");
var model = require('./../models');
var agenda = model.Agenda;
var ticket = model.Ticket;
var op = sequalize.Op;

module.exports.create = (req, res, next) => {
    agenda.create(req.body).then(ag => {
        return res.status(200).json({resultat: ag});
    })
}

module.exports.add = (req, res , next) => {
    var all_agenda = agenda.findAll({
        limit: 1,
        order: [
            [sequalize.col('end'), 'DESC']
        ],
    }).then((result) => {
        //var time = Date.now();
        var time = new Date();
        console.log(time);
        if (result[0] != null && result[0].end > time) {
             time = result[0].end;
        } else {
        }

        //var date_string = time.toDateString();
        var day = time.getDay();
        var hours = time.getHours();
        if (day >= 5 && hours >= 18)
        {
            time = time + (60*60*63) * 1000;
        }
        else if (hours >= 18) {
            time = time + (60*60*15) * 1000;
        }

        var time_end = new Date(time)   ;
        time_end.setHours(hours +1);
        console.log(time_end);
        let model = {
            start: time,
            end: time_end,
            TicketId: req.body.id
        }
        agenda.create(model).then((result) => {
            return  result;
        }).catch((err) => {
            console.log(err);
            return err;
        })
    }).catch((err) => {
        console.log(err);
        return err;
    });
};

module.exports.get = (req, res , next) => {
    console.log(req.params.id);
    var date = new Date();
    date.setHours(0,0,0,0);
    var where = {
        end:{[op.gte]: date}
    }
    agenda.findAll({
        where: where,
        include: [{model: ticket}]
    }).then( result => {
        //console.log(result);
        res.status(200).json(result);
    }).catch(err => {
        res.status(500).json(err);
    });
};