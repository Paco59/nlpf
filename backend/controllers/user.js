var model = require('./../models');
var user = model.User;
var auth = require('./auth');
var _ = require('lodash');

module.exports.update = (req, res, next) => {
    var model = req.body;
    if (_.isEqual(model.password, model.passwordConfirm)) {
        var saltPwd = auth.saltHashPassword(model.password);
        model.password = saltPwd.passwordHash;
        model.salt = saltPwd.salt;
        delete model.passwordConfirm;
    }
    else {
        delete model.password;
        delete model.passwordConfirm;
    }
    user.update(req.body, { where: { id: req.body.user.id } }).then(result => {
        res.status(200).json(result);
    }).catch(err => {
        res.status(500).json(err);
    });
};

module.exports.list = (req, res, next) => {
    user.findAll().then(result => {
        res.status(200).json(result);
    }).catch(err => {
        res.status(500).json(err);
    });
};
module.exports.blacklist = (req, res, net) => {
    var users = req.params;
    users.blacklisted = true;
    user.findById(parseInt(req.params.id, 10)
    ).then((result) => {
        result.blacklisted = !result.blacklisted;
        result.save().then((o) => {
            return res.status(200).json({result: _.omit(o.get(), ['password', 'salt', 'createdAt', 'updatedAt'])});
        })
    }).catch((err) => {
        return res.status(500).send(err);
    })
};

module.exports.info = (req, res, next) => {
    user.findById(req.body.user.id).then(result => {
        return res.status(200).json(result);
    }).catch(err => {
        return res.status(500).json(err);
    });
};
