'use strict';

var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var _ = require('lodash');

var models = require('./../models');
var User = models.User;

var genRandomString = (length) => {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex')
        .slice(0, length);
}

var sha256 = (password, salt) => {
    var hash = crypto.createHmac('sha256', salt);
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
}

var saltHashPassword = (userPassword) => {
    var salt = genRandomString(16);
    return sha256(userPassword, salt);
}
module.exports.saltHashPassword = saltHashPassword;

module.exports.isAuthenticated = (req, res, next) => {
    if (req.headers.authorization) {
        jwt.verify(req.headers.authorization.split(' ')[1], 'NLPF', (err, decoded) => {
            if (err) return res.status(500).json({
                error: 'Not authenticated'
            });
            req.body.user = jwt.decode(req.headers.authorization.split(' ')[1]);
            return next();
        });
    } else return res.status(500).json({
        error: 'Not authenticated'
    });
}

module.exports.signIn = (req, res, next) => {
    var creds = req.body;
    var valuesToCheck = ['email', 'password'];
    var validSignIn = function (creds) {
        var errKeys = [];
        _.forEach(valuesToCheck, (key) => {
            if (!_.has(creds, key)) errKeys.push(key);
        });
        return {
            valid: _.isEmpty(errKeys),
            err: errKeys,
        };
    }(creds);
    if (validSignIn.valid) {
        User.findOne({
            where: {
                email: creds.email // Check if email exists
            }
        }).then((user) => {
            if (_.isNull(user)) { // No User found
                return res.status(500).json({
                    error: 'Email and/or password are incorrects'
                });
            } else {
                var user = user.get();
                var saltPwd = sha256(creds.password, user.salt); // Check password with user'salt
                if (_.isEqual(saltPwd.passwordHash, user.password)) { // Correct password
                    var token = jwt.sign(_.omit(user, ['password', 'salt', 'createdAt', 'updatedAt']), 'NLPF');
                    res.set("authorization", token); // Set in header authorization token
                    return res.status(200).json({
                        data: 'Login Successful',
                        user: _.omit(user, ['password', 'salt', 'createdAt', 'updatedAt']),
                        token: token
                    });
                } else { //Wrong Password
                    return res.status(500).json({
                        error: 'Email and/or password are incorrects'
                    });
                }
            };
        })
    } else { // Missing signIn data
        return res.status(500).json({
            error: 'Missing properties: ' + _.concat(validSignIn.err)
        });
    }

}

module.exports.signUp = (req, res, next) => {
    var creds = req.body;
    var valuesToCheck = ['name', 'surname', 'phone', 'email', 'password', 'passwordConfirm'];
    var validSignUp = function (creds) {
        var errKeys = [];
        _.forEach(valuesToCheck, (key) => {
            if (!_.has(creds, key)) errKeys.push(key);
        });
        return {
            valid: _.isEmpty(errKeys),
            err: errKeys,
        };
    }(creds);
    if (validSignUp.valid && _.isEqual(creds.password, creds.passwordConfirm)) { // Check validity and corresponding password and conform
        User.findOne({
            where: {
                email: creds.email
            }
        }).then((user) => {
            if (_.isNull(user)) { // User non existant, creating it
                var model = creds;
                var saltPwd = saltHashPassword(model.password);
                model.password = saltPwd.passwordHash;
                model.salt = saltPwd.salt;
                User.create(model).then((newUser) => {
                    var ret = _.omit(newUser.get(), ['password', 'salt']);
                    return res.status(200).send({
                        data: ret
                    });
                })
            } else return res.status(500).json({
                error: 'Email already in use'
            });
        })
    } else { // Missing properties for signUp
        return res.status(500).json({
            error: 'Error while confirming password or Missing properties: ' + _.concat(validSignUp.err)
        });
    }
}