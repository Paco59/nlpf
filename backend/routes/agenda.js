var express = require('express');
var router = express.Router();
var ctrl = require('./../controllers');
var agenda = ctrl.agenda;
var auth = ctrl.auth;


var path = '/agenda';
/* GET users listing. */
router.get('/list', auth.isAuthenticated,agenda.get);
router.get('/:id', auth.isAuthenticated,agenda.get);
router.post('/add', auth.isAuthenticated, agenda.create);


module.exports = {path: path, router: router};
