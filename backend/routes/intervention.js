var express = require('express');
var router = express.Router();
var ctrl = require('./../controllers');
var inter = ctrl.intervention;
var auth = ctrl.auth;
var p = require('path');

var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });

var path = '/intervention';
/* GET users listing. */
router.post('/add', upload.single('file'), auth.isAuthenticated,inter.create);
router.get("/list", auth.isAuthenticated, inter.list);
router.put("/refused", auth.isAuthenticated, inter.refused);
router.put('/update', auth.isAuthenticated, inter.edit);
router.put("/accept", auth.isAuthenticated, inter.accepted);
router.put("/end", auth.isAuthenticated, inter.end);

router.get("/:image",(req, res, next) => {
  var options = {
    root: __basedir + '/uploads/',
    dotfiles: 'deny',
    headers: {
      'x-timestamp': Date.now(),
      'x-sent': true
    }
  };

  var fileName = req.params.image;

  res.sendFile(fileName, options, function (err) {
    if (err) {
      next(err);
    } else {
      console.log('Sent:', fileName);
    }
  });
});
module.exports = {path: path, router: router};
