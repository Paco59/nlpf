var express = require('express');
var router = express.Router();
var ctrl = require('../controllers');
var auth = ctrl.auth;
var user = ctrl.user;

var path = '/user';
/* GET users listing. */
router.get('/',auth.isAuthenticated, user.info);
router.get('/list',auth.isAuthenticated, user.list);
router.put('/update',auth.isAuthenticated, user.update);
router.put('/blacklist/:id',auth.isAuthenticated, user.blacklist);

module.exports = {path: path, router: router};