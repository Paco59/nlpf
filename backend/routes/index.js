/**
 * The purpose of this index.js to require all controllers files in one.
 */

var routes = {};

// Add filename of controllers
var files = ['users', 'auth', 'intervention', 'agenda'];

for (var i = 0; i < files.length; i++) {
    routes[files[i]] = require('./' + files[i]);
}

module.exports = routes;