var express = require('express');
var router = express.Router();
var auth = require('./../controllers').auth;

// Auth route: /auth/*
var path = '/auth';

//SignUp
router.post('/signup', auth.signUp)
// SignIn
router.post('/signin', auth.signIn)


module.exports = {path: path, router: router};
